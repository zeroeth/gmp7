# README #

### What is this repository for? ###

* Generative Modeling Project(GMP) is automatic modeling system on Blender.
* version - GMP 7

### How do I get set up? ###

* need [LoopTools] add-on
* set the PASS to the [samples] folder in line #16 in the code
* run the script in [TEXT edit] window
* see [Misc] tab in the tool shelf
* enter 'sample1'(or other file in the [samples] folder) into the [seed] text field
* push [Generate] button!

### Contribution guidelines ###

* By and No Commercial Use in CC

![by-nc.png](https://bitbucket.org/repo/eEG7Bj/images/4134267630-by-nc.png)

### Who do I talk to? ###

* Shigeto Maeda(Shige)
* s.maeda@oud-japan.co.jp
* http://sigma6289.tumblr.com
